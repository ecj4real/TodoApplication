import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import Router from 'vue-router'
import TodoApp from '@/components/todo'
import TodoItem from '@/components/todoitem'
import Register from '@/components/register'

Vue.use(BootstrapVue)
Vue.use(Router)


export default new Router({
  routes: [
    {
      path: '/',
      name: 'TodoApp',
      component: TodoApp
    },
    {
      path: '/todo',
      name: 'TodoItem',
      component: TodoItem
    },
    {
      path: '/register',
      name: 'Regiter',
      component: Register
    } 
  ]
})
