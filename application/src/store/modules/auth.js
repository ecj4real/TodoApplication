import axios from 'axios'

const state = {
    error: false,
    token: null,
    loading: false,
    isAuthenticated: false
}
const getters = {
    error: state => state.error,
    token: state => state.token,
    loading: state => state.loading,
    isAuthenticated: state => state.isAuthenticated
}
const actions = {
    register({commit, state}, args){
        axios.post('http://localhost:3333/register',{
            'firstname': args.firstname,
            'lastname': args.lastname,
            'email': args.email,
            'password': args.password
        }).then(function(response){
            console.log(response);
            console.log('Registered')
        }).catch(function(err){
            console.log(err);
            console.log("Error");
        })
    },
    //login
}
const mutations = {
    loading(state){
        
    },
    notLoading(state){
        
    },
    setError(state){
        
    },
    clearError(state){
        
    },
    setToken(state, value){
        
    },
    isAuthenticated(state){
        
    },
    notAuthenticated(state){
        
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}